import React from 'react'
import { connect } from 'react-redux'
import { loadData } from './Actions'

class Info extends React.Component {

    componentDidMount() {
        this.props.loadData();
    }

    render() {
        if (this.props.isFetching) {
            return (
                <div>Carregando...</div>
            )
        }

        if (this.props.error) {
            return (
                <div>Ocorreu um erro!</div>
            )
        }

        return (
            <div>
                <h3>Informações</h3>
                <p>{this.props.data.origin}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isFetching: state.ip.isFetching,
        data: state.ip.data,
        error: state.ip.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadData: () => dispatch(loadData())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Info);
