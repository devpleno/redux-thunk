import { loadUAData } from './Actions'

describe('test actions', () => {
    test('loadUAData', (done) => {
        const getMock = jest.fn()

        const dataMock = {}
        getMock.mockReturnValue(Promise.resolve({ dataMock }))

        let callNumber = 0

        const dispatch = jest.fn(params => {
            if (callNumber === 0) {
                expect(params).toEqual({ type: 'LOAD_UA_DATA_REQUEST' })
            }

            if (callNumber === 1) {
                expect(params).toEqual({ type: 'LOAD_UA_DATA_SUCCESS', data: dataMock['data'] })
                done()
            }

            callNumber++
        })

        const axiosMock = {
            get: getMock
        }

        loadUAData(axiosMock)(dispatch)
    })
})