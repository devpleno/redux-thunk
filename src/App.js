import React, { Component } from 'react';
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'

import reducer from './reducers/index'
import Info from './Info'
import UserAgent from './UserAgent'

const store = createStore(reducer, applyMiddleware(thunk, logger))

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Info />
          <UserAgent />
        </div>
      </Provider>
    );
  }
}

export default App;
