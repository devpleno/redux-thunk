import React from 'react'
import { connect } from 'react-redux'
import actions from './Actions'

class UserAgent extends React.Component {

    componentDidMount() {
        this.props.loadUAData();
    }

    render() {
        if (this.props.isFetching) {
            return (
                <div>Carregando...</div>
            )
        }

        if (this.props.error) {
            return (
                <div>Ocorreu um erro!</div>
            )
        }

        return (
            <div>
                <h3>UserAgent</h3>
                <p>{this.props.data['user-agent']}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isFetching: state.ua.isFetching,
        data: state.ua.data,
        error: state.ua.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadUAData: () => dispatch(actions.loadUAData())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAgent);
