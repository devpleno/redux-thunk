import axios from 'axios'

export const loadDataRequest = () => {
    return {
        type: 'LOAD_DATA_REQUEST'
    }
}

export const loadDataError = () => {
    return {
        type: 'LOAD_DATA_ERROR'
    }
}

export const loadDataSuccess = (res) => {
    return {
        type: 'LOAD_DATA_SUCCESS',
        data: res['data']
    }
}

export const loadData = () => {
    return dispatch => {
        dispatch(loadDataRequest());

        axios.get('http://httpbin.org/ip')
            .then((res) => dispatch(loadDataSuccess(res)))
            .catch(() => dispatch(loadDataError()))
    }
}




export const loadUADataRequest = () => {
    return {
        type: 'LOAD_UA_DATA_REQUEST'
    }
}

export const loadUADataError = () => {
    return {
        type: 'LOAD_UA_DATA_ERROR'
    }
}

export const loadUADataSuccess = (res) => {
    return {
        type: 'LOAD_UA_DATA_SUCCESS',
        data: res['data']
    }
}

export const loadUAData = (axios) => {
    return dispatch => {
        dispatch(loadUADataRequest());

        axios.get('http://httpbin.org/user-agent')
            .then((res) => dispatch(loadUADataSuccess(res)))
            .catch(() => dispatch(loadUADataError()))
    }
}

export default {
    loadUAData: loadUAData.bind(null, axios)
}